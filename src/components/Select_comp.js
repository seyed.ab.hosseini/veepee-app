import React, { Component } from 'react'
import RRS from 'react-responsive-select';
import { CaretIcon } from "./CaretIcon";
import { MultiSelectOption } from "./MultiSelectOption";
import { destinations } from '../destinations'



import "react-responsive-select/dist/ReactResponsiveSelect.css";
import "../style/rss.scss";



let countryOptions = [{
    value: "null",
    text: "Tous",
    markup: <MultiSelectOption>Tous</MultiSelectOption>
}];
for (const destination of destinations) {
    if (!countryOptions.find(obj => obj.text === destination.country)) {
        countryOptions.push({
                    value: destination.country.toLowerCase(),
                    text: destination.country,
                    markup: <MultiSelectOption>{destination.country}</MultiSelectOption>
            });
    }
}


export default class SelectItem extends Component {
    state = {
        make2: {
          options: [
            {
              value: countryOptions[0].value,
              text: countryOptions[0].country
            }
          ]
        }
      };
    
    handleChange = newValue => {
        const { options } = newValue;
        if (options.length) {
          const name = options[0].name;
          const nextFormValue = {
            [name]: {
              options: options.map(({ text, value }) => ({
                text,
                value
              }))
            }
          };
          this.setState({
            ...this.state,
            ...nextFormValue
          });
          this.props.callback(nextFormValue.make2.options)
        }
      };
    


    render() {
        const { make2 } = this.state;

        return (
            <div>
                <form>
                    <RRS
                         key="make2"
                         multiselect
                         name="make2"
                         options={countryOptions}
                         caretIcon={<CaretIcon key="2" />}
                         selectedValues={make2.options.map(option => option.value)}
                         onChange={this.handleChange}
                    />
                </form>
            </div>
        )
    }
}
