import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <div className="footer-container">
                <div className="row">
                    <div className="col-12 ">
                        <div className="row">
                            <div className="col-12 uppercase">
                            rejoignez <span className="fontBlack">emirates | the list</span>
                            </div>
                            <div className="col-12 uppercase">
                                vol + hôtel négociés jusqu'à -70%
                            </div>
                            <hr className="footer-hr"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
