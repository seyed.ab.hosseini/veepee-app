import React, { Component } from 'react'
import '../style/App.scss';


export default class Header extends Component {
    render() {
        return (
            <div className="container">
                <div className="row mt10">
                    <div className="col-sm-12">
                    <h1 className="uppercase fontBook red">découvrez les offres du moment</h1>
                    <h2 className="uppercase fontLight">vol + hôtel jusqu'à -70%</h2>
                    <hr className="title-hr" />
                    </div>
                </div>
            </div>
        )
    }
}
