import React, { Component } from 'react'

export default class Member extends Component {
    render() {
        return (
            <div className="container member-container">
                <div className="row">
                    <div className="col-12 uppercase fontLight">
                        comment bénéficier de l'offre ?
                    </div>
                    <div className="col-12 uppercase fontBold">
                        150€ de réduction* dès 1000€ d'achat
                    </div>
                </div>
                <hr className="member-hr" />

                <div className="row">
                    <div className="col-12 red-container">
                        <div className="fontBold">Déjà membre ?</div>
                        <div className="small">Votre code promo vous attend directement sur le site <a href="/" className="fontBlack">en cliquant ici</a></div>
                    </div>

                    <div className="col-12 uppercase">
                        <div className="row">
                            <div className="col-12 col-lg-4">
                                <div className="row">
                                    <div className="col-3 bigNumber"> <span>1</span></div>
                                    <div className="col-9 numberLabel mt14">
                                        <div className="row small">rejoignez</div>
                                        <div className="row fontBold red small">emirates | the list</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-lg-4">
                                <div className="row">
                                    <div className="col-3 bigNumber"><span>2</span></div>
                                    <div className="col-9 numberLabel mt10">
                                        <div className="row small">recevez par email</div>
                                        <div className="row fontBold red small">votre bon de 150€ offert</div>
                                        <div className="row small">à utiliser dès 1000€ d'achat</div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-lg-4">
                                <div className="row">
                                    <div className="col-3 bigNumber"><span>3</span></div>
                                    <div className="col-9 numberLabel mt14">
                                        <div className="row small">réservez votre séjour</div>
                                        <div className="row fontBold red small">avant le 30 avril 2019</div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>
                
                </div>
                <div className="row">
                    <div className="col-12">
                    <button className="btn btn-member uppercase">je m'inscris</button>
                    </div>
                </div>
            </div>
        )
    }
}
