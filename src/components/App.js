import React from 'react';
import Header from './Header_comp'
import Member from './Member_comp'
import Destinations from './Destinations_comp'
import Footer from './Footer_comp'

import '../style/App.scss';

function App() {
  return (
    <div className="App">
      <Header />
      <Destinations />
      <Member />
      <Footer />
    </div>
  );
}

export default App;
