import React, { Component } from 'react'
import { destinations } from '../destinations'
import DestinationsItem from './Destinations_item'
import SelectItem from './Select_comp'



export default class Destinations extends Component {
    constructor(props) {
        super(props)
        this.state = { destinations: destinations }
      }

      receiveCallBack = (countryList) => {
        if (countryList[0].text === "Tous") {
            this.setState({
                destinations:destinations
            });
        } else {
            let newDestinations = [];
            for (let item of countryList) {
                for (let destination of destinations) {
                    if (destination.country === item.text) {
                        newDestinations.push(destination);
                    }
                }
            }
            this.setState({
                destinations:newDestinations
            });
        }
    }

    render() {
        return (
            <div className="container">
                <div className="multiselect-container">
                    <SelectItem callback={this.receiveCallBack}/>
                </div>
                <div className="row">
        {this.state.destinations.map( (item, index)  => 
            <div className="col-lg-4 col-12" key={index}><DestinationsItem item={item}/></div> 
            )}
                </div>
            </div>
        )
    }

    shouldComponentUpdate (nextProps, nextState) {
        console.log(nextState);
        console.log(nextProps);

        if(!nextState){
            return false;
        }else{
            return true;
        }
    }


}
