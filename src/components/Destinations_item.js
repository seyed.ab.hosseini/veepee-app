import React, { Component } from 'react'
import { MdRemove } from 'react-icons/md';
import { IoIosArrowForward } from 'react-icons/io';
import SimpleSlider from './Slide_comp';

import CLOS_DU_LITTORAL from '../images/CLOS_DU_LITTORAL.png';
import JAPON from '../images/JAPON.jpg';
import ECHAPEE_SRI_LANKAISE from '../images/ECHAPEE_SRI_LANKAISE.png';
import ENTRE_CULTURE_ET_PLAGES from '../images/ENTRE_CULTURE_ET_PLAGES.png';
import FAIRMONT_DUBAI from '../images/FAIRMONT_DUBAI.png';
import HYATT_REGENCY_CREEK from '../images/HYATT_REGENCY_CREEK.png';
import IMPIANA_RESORT_SAMUI from '../images/IMPIANA_RESORT_SAMUI.png';
import LAGUNA_BEACH from '../images/LAGUNA_BEACH.png';
import NOKU_MALDIVES from '../images/NOKU_MALDIVES.png';
import STHALA_MARC_PATRA from '../images/STHALA_MARC_PATRA.png';


const slideFilter = [
    "Laguna Beach Hotel & Spa", 
    "Impiana Resort Samui", 
    "Grand Arc Hanzomon"
];

const properPicture = {
    "Hyatt Regency Creek": HYATT_REGENCY_CREEK,
    "Laguna Beach Hotel & Spa": LAGUNA_BEACH,
    "Fairmont Dubaï": FAIRMONT_DUBAI,
    "Combiné Sthala - Marc - Patra": ENTRE_CULTURE_ET_PLAGES,
    "Noku Maldives": NOKU_MALDIVES,
    "Impiana Resort Samui": IMPIANA_RESORT_SAMUI,
    "Clos du Littoral": CLOS_DU_LITTORAL,
    "Echappée SriLankaise": ECHAPEE_SRI_LANKAISE,
    "Grand Arc Hanzomon": JAPON,
    "Entre Culture et Plages": STHALA_MARC_PATRA
};

export default class DestinationsItem extends Component {
    
    render() {
        const {item} = this.props;
        let rating = " ";
        for (let i=0; i < parseInt(item.rating.charAt(0)); i++) {
            rating += "*";
        }
        return (
            <div className="list-container">
                <div className="img-container">
                    {slideFilter.includes(item.label) ? <SimpleSlider labelIn={item.label}/> : <div>
                        <img src={properPicture[item.label]} alt={item.label} />
                        </div>
                    }
                    <div className="bottom-right fontBold">{item.upto}</div>
                </div>
                <div className="info-container">
                    <div className="arrow-container">
                        <IoIosArrowForward />
                    </div>
                    <div className="text-area-container">
                        <div className="">
                            <span className="fontBold">{item.country}</span>  <span className="sep-container"><MdRemove /></span> <span className="fontLight">{item.place}</span>
                        </div>
                        <div className="">
                            <span className="fontLight small">{item.label}</span>
                            <span className="fontBold">{rating}</span>
                        </div>
                        <div className="tag-container fontLight uppercase">
                            {item.tags.map( (tag, index) => <span className={tag.classname === "option" ? "tag-label-gold": "tag-label-dark"} key={index}>{tag.label}</span> )}
                        </div>
                    </div>
              
                </div>
            </div>
        )
    }
}
