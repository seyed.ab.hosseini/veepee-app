import React, {Component} from 'react';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

import ECHAPEE_SRI_LANKAISE from '../images/ECHAPEE_SRI_LANKAISE.png';
import CLOS_DU_LITTORAL from '../images/CLOS_DU_LITTORAL.png';
import ENTRE_CULTURE_ET_PLAGES from '../images/ENTRE_CULTURE_ET_PLAGES.png';
import FAIRMONT_DUBAI from '../images/FAIRMONT_DUBAI.png';
import HYATT_REGENCY_CREEK from '../images/HYATT_REGENCY_CREEK.png';
import JAPON from '../images/JAPON.jpg';
import LAGUNA_BEACH from '../images/LAGUNA_BEACH.png';
import NOKU_MALDIVES from '../images/NOKU_MALDIVES.png';
import STHALA_MARC_PATRA from '../images/STHALA_MARC_PATRA.png';


 
const slideImages = [
    {
        label: "Impiana Resort Samui",
        pics: [
            ECHAPEE_SRI_LANKAISE,
            CLOS_DU_LITTORAL,
            ENTRE_CULTURE_ET_PLAGES
        ]
    },
    {
        label: "Grand Arc Hanzomon",
        pics: [
            FAIRMONT_DUBAI,
            HYATT_REGENCY_CREEK,
            JAPON
        ]
    },
    {
        label: "Laguna Beach Hotel & Spa",
        pics: [
            LAGUNA_BEACH,
            NOKU_MALDIVES,
            STHALA_MARC_PATRA
        ]
    }
];
 


export default class SimpleSlider extends Component {
    render() {
      const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable: true,
        pauseOnHover: true
      };
      const {labelIn} = this.props;
      return (
        <Slider {...settings}>
        {slideImages.find(({ label }) => label === labelIn).pics.map((pic, i) => 
            
              <img src={pic} alt="pic" key={i}/>
            
        )}

        </Slider>
      );
    }
  }